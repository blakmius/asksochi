define(['./cito'], cito => {
Object.entries = x => Object.keys(x).map(i=>[i, x[i]]);
const fval = (f, ctx) => (typeof f === 'function') ? fval(f(ctx), ctx) : f;
const throttled = (delay, fn) => {
    let timeout;
    const wrap = () => {
        clearTimeout(timeout);
        fn();
    }
    return force => force ? wrap() : timeout = setTimeout(wrap, delay);
}
const callQueue = q => { while (q.length > 0) q.shift()(); }
const concat = (a, b) => Array.isArray(a) && Array.isArray(b) ? a.concat(b) : 
    typeof a === 'object' && typeof b === 'object' ? Object.assign(a, b) : b;

const specre = /([0-9a-z\-_]+|[<!])|([#\.]?[0-9a-z\-_]+)/gi;

const node = (dom, func) => {
    let vnode, queue = [], updating = false, ctx;
    const performUpdate = throttled(0, () => {
        updating = true;
        if (vnode) cito.vdom.update(vnode, func(ctx));
        else vnode = cito.vdom.append(dom, func(ctx));
        callQueue(queue);
        ctx = {update};
        updating = false;
    });
    const update = (arg, nctx={}) => {
        ctx = Object.assign(ctx, nctx);
        let force = false;
        if (typeof arg === 'function') queue.push(arg);
        else if (arg === true) force = true;
        if(!updating) performUpdate(force);
    }
    ctx = {update};
    performUpdate(true);
    return {vnode, update, set: f => func = f};
}

const flatten = (array, func, ctx) => [].concat(...array.map(elem => func(elem, ctx)));

const toStringList = (v, ctx) => (v !== 0 && !v) ? [] :
    (typeof v === 'string') ? [v] :
    (typeof v === 'function') ? toStringList(v(), ctx) :
    (typeof v === 'object') ?
        Array.isArray(v) ? flatten(v, toStringList, ctx) :
        fval(()=>{
            let result = [];
            Object.entries(v).forEach(
                ([key, val]) => (v.hasOwnProperty(key) && fval(val)) ? result.push(key) : undefined);
            return result;
        }):
    [String(v)];

const Children = (c, ctx) => (c === undefined || c === null) ? '' :
    (typeof c === 'number') ? String(c) :
    (typeof c === 'function') ? Children(c(), ctx) :
    (Array.isArray(c)) ? flatten(c, Children, ctx) : c;

const parseSpec = (spec, ctx) => {
    spec = fval(spec, ctx);
    if (typeof spec === 'string') {
        if (['<', '!'].indexOf(spec) != -1) return {tag: spec};
        else if (spec === '') return {};
        else spec = {is: spec};
    }
    if (typeof spec !== 'object') return {};
    spec.is = fval(spec.is, ctx);
    let result = {tag: 'div', attrs: {}, events: {}}, classes = [];
    if (spec.is && typeof spec.is === 'string') (spec.is.match(specre) || []).forEach(m => {
        if (m.charAt(0) == '.') classes.push(m.substr(1));
        else if (m.charAt(0) == '#') result.attrs.id = m.substr(1);
        else result.tag = m;
    });
    else result = spec.is;
    Object.entries(spec).forEach(([key, val]) => {
        if (!spec.hasOwnProperty(key) || key === 'is') return;
        if (key.substr(0, 2) === 'on') result.events[key.substr(2)] = concat(result.events[key.substr(2)], Array.isArray(val) ? val : [val]);
        else {
            let nval = fval(val, ctx);
            if (nval === undefined) return;
            if (key === 'key') result.key = nval;
            else if (key === 'class') classes = classes.concat(toStringList(nval, ctx));
            else result.attrs[key] = concat(result.attrs[key], nval);
        }
    });
    if (classes.length > 0) result.attrs['class'] = (result.attrs['class'] || '').split(' ').concat(classes).join(' ').trim();
    return result;
}

const z = (spec, ...children) => ctx => {
    let result = parseSpec(spec, ctx);
    result.children = concat(result.children, Children(children, ctx));
    return result;
}

const Val = v => ({
    get: () => v,
    set: vv => v = v
});

const Ref = (obj, prop) => ({
    get: () => obj[prop],
    set: v => obj[prop] = v
});

return Object.assign(z, {node, Ref, Val, throttled, callQueue});
});