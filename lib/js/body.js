define(['./zombular'], z => {

let _route, _args;
let body = z.node(document.body, z(''));

const route = (r, args) => {
    if (r === undefined) return {route: _route, args: _args};
    if (r === null) { 
        args = Object.assign({}, _args, args);
        r = _route;
    }
    let result = [r];
    Object.entries(args).forEach(([k, v]) => {
        if (v !== undefined)
            result.push(encodeURIComponent(k) + '=' + encodeURIComponent(v));
    });
    return '#'+result.join(';');
}

const updateRoute = () => {
    let hash = window.location.hash.slice(1), pairs;
    [_route, ...pairs] = hash.split(';');
    _args = {};
    pairs.forEach(p => {
        let [key, ...values] = p.split('=');
        _args[decodeURIComponent(key)] = decodeURIComponent(values.join('='));
    });
    body.update();
}

window.addEventListener('hashchange', updateRoute);
updateRoute();

return Object.assign((...args) => z(...args), z, {
    setBody: f => { body.set(f); body.update(); },
    update: body.update,
    route
});

});
