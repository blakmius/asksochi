define([
	'lib/js/body'
], gi => (label, value, props) => {
if(!props) props = {};
var isvalid = true;
var valid = () => {if(props.validation) props.isvalid = props.validation(value.get())};
valid();
if(props.isvalid !== undefined && props.touched) isvalid = props.isvalid;
var inputClass = () => "gi-input"
 + (props.floatLabel? " floatLabel": "")
 + (props.disabled? " disabled": "")
 + (props.focused? " focused": "")
 + (props.touched? " touched": "")
 + (value.get()? " dirty": "")
 + (isvalid? "": " invalid");
return () => gi({is: "div", class: inputClass, $changes: props.self},
    props.prefix? props.prefix: "",
    gi(".fill",
        gi({is: "div.label"}, label),
        gi("div.textfield-container",
            gi({is: "input", $changes: props.input,
                value: value.get() || "",
                type: props.type || "text",
                readonly: props.readonly || props.disabled || false,
                on$created: function(e) {
                    if(props.focused) setTimeout(()=>e.target.focus(), 100);
                },
                onfocus: function(e) {
                    props.focused = true && !props.disabled;
                    gi.update();
                },
                onblur: function(e) {
                    props.focused = false;
                    props.touched = true;
                    gi.update();
                },
                oninput: function(e) {
                    value.set(e.target.value);
                    valid();
                    props.touched = true;
                    gi.update();
                }
            }),
            props.postfix? props.postfix: ""
        ),
        gi("div.border"),
        gi("div.error-message", !isvalid? props.errorMessage || "": "")));
})