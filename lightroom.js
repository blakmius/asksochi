define([
	'lib/js/body'
], z => images => {

let index = 0,
	opened = false,
	targets = [];

const close = () => {
	opened = false;
	z.update();
}

const open = (i = 0) => {
	if (typeof i === 'string') index = images.indexOf(i);
	else index = i;
	opened = true;
	z.update();
}

const scroll = e => {
	let diff = (e.offsetLeft + e.clientWidth/2 - e.parentElement.clientWidth/2) - e.parentElement.scrollLeft,
	    j = 10,
	    interval_id = setInterval(() => {
	        e.parentElement.scrollLeft += diff/10;
	        j--;
	        if (!j) clearInterval(interval_id);
	    }, 30);
}

return Object.assign(() => z({is: '.modal.lightbox', class: {'is-active': opened}},
	z({is: '.modal-background', onclick: close}),
	z('.modal-content',
		z('.img',
			z('<', `<img class="showedImage" src="${images[index]}">`),
			z({is: '.arrow-left', onclick() {
				index = --index < 0 ? images.length-1 : index;
				scroll(targets[index]);
				z.update();
			}}, '❮'),
			z({is: '.arrow-right', onclick() {
				index = ++index%images.length;
				scroll(targets[index]);
				z.update();
			}}, '❯')
		),
		z('.thumbs.sp1',
			images.map((i, n) => z({is: 'img',
				on$created: e => targets.push(e.target),
				onclick(e) {
					open(i);
					scroll(e.target);
				},
				class: { selected: index==n },
				src: i })
			)
		),
	),
	z({is: 'button.modal-close.is-large', onclick: close})
), { open, close });

});