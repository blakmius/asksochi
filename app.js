require([
	'lib/js/body',
	'lib/js/smoothscroll',
	'input',
	'lightroom'
], (z, smoothScroll, Input, Lightroom) => {

const breakpoints = {
	xlarge: '(max-width: 1800px)',
	large: '(max-width: 1280px)',
	medium: '(max-width: 980px)',
	notSmall: '(min-width: 769px)',
	small: '(max-width: 769px)',
	howWeWorks: '(max-width: 576px)',
	notHowWeWorks: '(min-width: 576px)',
	xsmall: '(max-width: 480px)'
}

const breakpoint = (a, b) => `@media screen and ${breakpoints[a]} {${b}}`;

const Style = z('style', `
body { font-family: 'Open Sans', sans-serif; background: #f7f7f7; }
.roboto { font-family: 'Roboto', sans-serif; }

.bg0, .bg1 { background-color: #fff!important; background-repeat: no-repeat, repeat, no-repeat; background-size: cover; background-position: center;
	background-attachment: fixed; }

.bg0 { background-image: -webkit-radial-gradient(center, circle cover, rgba(0, 0, 0, 0.3) 0, rgba(0, 0, 0, 0.5) 80%),
	url(assets/bg.jpg); }
.bg1 { background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.3)), url(assets/bg1.jpeg); }
.hero.bg2 { background-color: #00bcd4; }

.hero.is-primary .nav { box-shadow: none; }
.nav-item { margin: .5rem 2rem; padding: 0; border-bottom: dotted 1px #fff; }

.nav-right.nav-menu { justify-content: center; }

@keyframes slideInDown {
  from { transform: translate3d(0, -100%, 0); }
  to { transform: translate3d(0, 0, 0); }
}

.hero-head.is-active > .nav, .nav-right.nav-menu.is-active { background-color: rgba(0, 0, 0, 0.85)!important; }
.nav-menu.is-active { animation: slideInDown 0.2s ease; }
.nav-right.nav-menu.is-active > .nav-item { border-top: none; }

.uc { text-transform: uppercase; }

.header { padding-bottom: 45px; }

.sp1 { margin-top: 15px; }
.sp2 { margin-top: 30px; }
.sp3 { margin-top: 45px; }
.sp4 { margin-top: 60px; }

.text { white-space: pre-wrap; word-wrap: break-word; max-width: 65em; display: inline-block; }
.text.small { max-width: 45em; }

.steps { position: relative; width: 576px; margin: auto; }
.steps .divider { position: absolute; border-left: 1px solid #000; height: 100%; left: calc(50% - 1px); }

.step { width: 288px; }

.step-icon { width: 128px; height: 128px; line-height: 128px; border-radius: 50%;
	text-align: center; border: 1px solid #000; font-size: 64px; margin: auto; color: #00bcd4; }

.step-num { padding: 6px; border: 1px solid #000; display: inline-block; }

.step-line {  left: calc(50% + 64px); position: relative; border-top: 1px dashed #000;
	top: 64px; width: calc(50% - 64px); }
.step.right .step-line { left: 0px; }

.inputs { display: flex; flex-wrap: nowrap; }

${breakpoint('notHowWeWorks', `
	.step { text-align: right; }
	.step.right { margin-left: 287px; text-align: left; }

	.step .step-text { padding-right: 12px}
	.step.right .step-text { padding-left: 12px}
`)}

${breakpoint('howWeWorks', `
	.step { margin-left: auto; margin-right: auto; }
	.step-line { display: none; }
	.steps .divider { display: none; }
	.steps { width: auto; }
	.step .step-text { text-align: center; }
`)}

.is-flex.nw { flex-wrap: nowrap; }
.is-flex.w { flex-wrap: wrap; }
.is-flex.ac { align-items: center; }

.feature { height: 100%; }
.featureIcon { font-size: 48px; padding-right: 15px; color: #00bcd4; }

#map { width 100%; height: 75vh; }
.contactUs { max-width: 769px; background: #fff; margin: auto; margin-top: -30vh; position: relative; z-index: 400;
	box-shadow: 0 16px 24px 2px rgba(0,0,0,.14), 0 6px 30px 5px rgba(0,0,0,.12), 0 8px 10px -5px rgba(0,0,0,.2); }

.s0 { flex: auto; }

.msg { padding: 34px; color: #13416f; width: 100%; font-size: 18px; }
.msg h2 { font-size: 28px; }

.msg .send { cursor: pointer; color: #fff; font-size: 24px; background: #51A0D5; float: right; line-height: 55px;
	border-radius: 50%; width: 64px; height: 64px; transition: background .2s; text-align: center; }
.msg .send:hover { background: #58ADE7; }

.msg .send .fa { vertical-align: middle; }

.contactInformation { background: #13416f; color: #fff; padding: 34px; font-size: 15px; }
.contactInformation h2 { font-size: 28px; }

.contactInformation .fa { font-size: 28px; min-width: 50px; color: #6888a9; }
.contactInformation .social { transition: color .2s; }
.contactInformation .social:hover { color: #A7B4C1; cursor: pointer; }

.social2 { transition: color .2s; font-size: 24px; margin-right: 28px; }
.social2:hover { color: #cbcdd0; cursor: pointer; }

.gi-input .textfield-container {
    height: 20px;
}
 
.gi-input.disabled {
    border-bottom: 1px dashed #BDBDBD;
    pointer-events: none;
}
 
.gi-input.disabled .label,
.gi-input.disabled input {
    color: #E0E0E0;
}
 
.gi-input {
    height: 42px;
    border-bottom: 1px solid #9E9E9E;
    padding: 0;
    flex: 1;
    margin-bottom: 30px;  
}
 
.gi-input input {
    flex: 1;
    border: none;
    outline: none;
    z-index: 1;
    background-color: transparent;
    position: relative;
    bottom: -2px;
    width: 100%;
}
 
.gi-input.focused .border {
    width: 100%;
    visibility: visible;
}
 
.gi-input .border {
    visibility: hidden;
    width: 1px;
    height: 2px;
    bottom: -5px;
    position: relative;
    margin: auto;
    background-color: #5C6BC0;
}
 
.gi-input .border {
    -webkit-transition-duration: .2s;
    transition-duration: .2s;
    -webkit-transition-timing-function: cubic-bezier(.4,0,.2,1);
    transition-timing-function: cubic-bezier(.4,0,.2,1);
}
 
.gi-input .label {
    transition: all 0.2s;
}
 
.gi-input.focused .label {
    color: #7986CB;
}
 
.gi-input.focused.floatLabel .label,
.gi-input.dirty.floatLabel .label {
    top: 2px;
    font-size: 11.5px;
}
 
.gi-input.dirty:not(.floatLabel) .label {
    opacity: 0;
}
 
.gi-input .label {
    font-weight: 400;
    color: #757575;
    position: relative;
    top: 17px;
    height: 15px;
    display: block;
    font-size: 13px;
    margin: 0;
    padding: 0;
    z-index: 0;
}
     
.gi-input .error-message {
    color: #d50000;
    margin-top: 10px;
    font-size: 10px;
}
 
.gi-input.invalid.touched .border,
.gi-input.invalid.dirty .border {
    background-color: #d50000;
}
 
.gi-input.invalid:not(.disabled) {
    border-color: #d50000;
}
 
.gi-input.invalid:not(.disabled) .label {
    color: #d50000;
}

.sz0 { width: 1em; }

a { color: #51A0D5; text-decoration: none; transition: color .2s; }
a:hover { color: #58ADE7; }

.c0 { color: #A7B4C1; }

.bullet { width: 6px; height: 6px; background: #A7B4C1; margin: 12px;; border-radius: 50px;}

.work { margin: 3rem 0; transform: skewY(-15deg); overflow: hidden; position: relative; cursor: pointer; }
.work > img { transform: scale(1.5) skewY(15deg); }

.work:after { content: ''; width: 100%; height: 100%; top:0; left:0; position: absolute; transition: background .2s; }
.work:hover:after { background: rgba(0, 0, 0, 0.5); }

.work > i { z-index: 1; font-size: 28px; color: #fff; opacity: 0; position: absolute;
	top: 50%; left:50%; transform: translate(-50%, -50%) skewY(15deg); transition: opacity .2s; }
.work:hover > i { opacity: 1; }

.work.left { transform: skewY(15deg); }
.work.left > img { transform: scale(1.5) skewY(-15deg); }
.work.left > i { transform: translate(-50%, -50%) skewY(-15deg); }

${breakpoint('small', `
	.foot { text-align: center; }
	.foot .is-flex { justify-content: center; }
	.contactInformation { text-align: center; }
	.contactInformation .is-flex { justify-content: center; }
`)}

${breakpoint('xsmall', `
	.title.is-2 { font-size: 2rem; }
	.bullet { display: none; }
	.footContacts { display: block!important; }
	.footContacts > *:last-child { margin-top: 15px; display: block; }
`)}

.modal { z-index: 1000; }

.thumbs { user-select: none; display: flex; height: 150px; width: 100%;
	background: rgba(0, 0, 0, 0.5); align-items: center; overflow-x: auto; }
.thumbs::-webkit-scrollbar { display: none; }
.thumbs img { transition: .2s; max-height: 120px; margin: .75rem; filter: brightness(.6); cursor: pointer; }

.thumbs img.selected, .thumbs img:hover { filter: none; }

.arrow-right, .arrow-left { user-select: none; color: #fff; position: absolute; top: 50%; transition: opacity .2s; 
	padding: 16px; cursor: pointer; margin-top: -50px; font-size: 28px; transition: .6s ease; font-weight: bold; }
.arrow-right:hover, .arrow-left:hover { background: rgba(0, 0, 0, 0.8); }
.arrow-right { right: 0; border-radius: 3px 0 0 3px; }
.arrow-left { left: 0; border-radius: 0 3px 3px 0; }

.modal-content { overflow: visible; width: auto; margin: auto; }

.lightbox .img { position: relative; }
.lightbox .showedImage { display: block; margin: auto; }

`);

const splitBy = (arr, n) => {
	let a = [];
	while (arr.length > 0) a.push( arr.splice(0, n) );
	return a;
}

const splitOn = (arr, n) => {
	let a = [...Array(n)].map(i => []);
	arr.forEach((i, j) => a[j%n].push(i));
	return a;
}

let menu = false;

const navs = [
	{name: 'Преимущества', link: 'Features', timeout: 500},
	{name: 'Как мы работаем', link: 'HowWeWorks', timeout: 750},
	{name: 'Галерея',link: 'Gallery', timeout: 1000},
];

const Header = z('.hero.is-fullheight.is-primary.bg0',
	z({is: '.hero-head', class: ()=>({'is-active': menu})},
		z('.nav',
			z({is: 'span.nav-toggle',
				onclick() {
					menu=!menu;
					z.update();
				},
				class: ()=>({'is-active': menu})
			}, z('span'), z('span'), z('span')),
			z({is: '.nav-right.nav-menu', class: ()=>({'is-active': menu})},
				navs.map(({name, link, timeout}) => z({is: 'a.nav-item', onclick() {
					smoothScroll(link, timeout);
					menu = false;
					z.update();
				}}, name))
			)
		)
	),
	z('.hero-body',
		z('.container.has-text-centered',
			z('h3.title.is-2.roboto', 'Алюминиевые светопрозрачные конструкции'),
			z('h5.subtitle', 'Производство и монтаж фасадов, витражей, окон, дверей, гаражных ворот и рольставней')
		)
	)
);

const SectionHeader = name => z('.has-text-centered.header.title.roboto', name);

const infoText = `Компания «АСК» успешно работает на строительных рынках Краснодарского края, Абхазии, Крыма и Москвы в сфере проектирования, изготовления и монтажа алюминиевых светопрозрачных конструкций и вентилируемых фасадов.

Компания осуществляет оригинальные инженерные разработки, постоянно совершенствует процессы производства выпускаемых конструкций, работает над улучшением монтажных технологий.

Наша специализация – алюминиевые светопрозрачные конструкции (фасады, витражи, окна, двери, рольставни) любой формы и расцветки.
`

const Info = z('.hero.is-fullheight.bg1.is-primary',
	z('.hero-body',
		z('.container',
			z('h3.title.is-2.roboto', 'Быстро, качественно и за разумные деньги!'),
			z('.text.small.sp1', infoText)
		)
	)
);

const WhatWeDo = z('.section',

);

let features = splitBy([
	{icon: 'robots', text: 'Сборка металлопластиковых конструкций на автоматических линиях, и снижение человеческого фактора в значимых моментах производства.'},
	{icon: 'assembly-line', text: 'Собственное производство алюминиевых конструкций в Адлере'},
	{icon: 'warehouse', text: 'Поставщиками сырья и комплектующих являются только признанные на международном уровне производители, сотрудничество с которыми — залог качества готовой продукции.'},
	{icon: 'diploma', text: 'Сертификаты качества на выпускаемую продукцию.'},
	{icon: 'hourglass', text: 'Четкое соблюдение сроков заказа.'},
	{icon: 'recommended', text: 'Гарантия качества на производимую продукцию. В течение гарантийного срока специалисты сервисной службы при возникшей необходимости бесплатно отрегулируют фурнитуру, выполнят ремонт и замену вышедших из строя элементов.'},
	{icon: 'measuring', text: 'Точность геометрии конструкций'},
	{icon: 'presentation', text: 'Полный комплекс работ от замера и проекта до монтажа и ввода в эксплуатацию.'},
	{icon: 'delivery-truck', text: 'Выезд специалиста по замеру.'},
	{icon: 'laborers', text: 'Постоянный штат менеджеров, инженеров, замерщиков и монтажников. Наши специалисты, имеют за плечами многолетний опыт работы, знают все тонкости обработки материалов и успешно применяют свои знания на практике.'},
], 3);

const Features = z('#Features.section',
	SectionHeader('Преимущества нашей компании'),
	z('.container',
		features.map(i => z('.columns.sp2.is-centered',
			i.map(({icon, text}) => z('.column.is-4', z('.is-flex.nw.ac.feature',
				z(`i.flaticon-${icon}.featureIcon`),
				z('p', text)
			)))
		))
	)
);

const Garanty = z('.hero.is-large.bg2.is-primary',
	z('.hero-body',
		z('.container.has-text-right',
			z('h3.title.is-2.roboto', 'Гарантия 5 лет!'),
			z('.text.small.sp1', 'Гарантийный срок на изделия и произведенный монтаж составляет 5 лет с момента подписания Акта Приема-сдачи Заказчиком, если иное не указано в документах Завода изготовителя, в коммерческом предложении к настоящему договору.')
		)
	)
);

const steps = [
	{icon: 'discount-voucher', title: 'Предложение', text: 'Получив тех.задание от заказчика, компания выставляет коммерческое предложение'},
	{icon: 'contract', title: 'Договор', text: 'После согласования сторон заключается договор с предоплатой 80%'},
	{icon: 'delivery-truck', title: 'Выезд', text: 'Наш замерщик выезжает на объект'},
	{icon: 'roulette', title: 'Работа с заказом', text: 'Получив точный замер, инженер просчитывает заказ, согласовывает с заказчиком и запускает в работу'},
	{icon: 'factory', title: 'Изготовление', text: 'После поставки материалов, изготавливаются светопрозрачные конструкции'},
	{icon: 'tools', title: 'Монтаж', text: 'Производится монтаж конструкция на объекте'},
	{icon: 'hand-shake', title: 'Сдача', text: 'Сдача работы заказчику, окончательный расчет, подписание актов'},
];

const Step = ({icon, title, text}, num) => z({is: '.step.sp2', class: {right: num%2==0}},
	z('.step-num', '0'+num),
	z('.step-line'),
	z('.step-icon', z(`i.flaticon-${icon}`)),
	z('.step-text.sp2',
		z('h3.title.roboto', title),
		z('p', text)
	)
)

const HowWeWorks = z('#HowWeWorks.section',
	z('.container',
		SectionHeader('Как мы работаем'),
		z('.steps',
			z('.divider'),
			steps.map((s, i) => Step(s, ++i))
		)
	)
);

const works = ['work01.jpg', 'work02.jpg', 'work03.jpg', 'work04.jpg', 'work05.jpg', 'work06.jpg', 'work07.jpg',
	'work08.jpg', 'work09.jpg', 'work11.jpg', 'work12.jpg', 'work13.jpg', 'work14.jpg', 'work15.jpg',
	'work16.jpg', 'work17.jpg'].map(i=>'assets/gallery/'+i);

const WorkLightroom = Lightroom(works);

const Work = (image, n) => z({is: '.work', onclick: e => WorkLightroom.open(image), class: {left: n%2==1}},
	z('<', `<img src="${image}">`),
	z('i.flaticon-zoom-in')
);

const Gallery = z('#Gallery.section',
	z('.container',
		SectionHeader('Выполненные работы'),
		z('.columns', splitOn(splitOn(works, 4), 2).map(a => z('.column', z('.columns.is-mobile',
			a.map((c, j) => z('.column',
				c.map(w => Work(w, j))
			))
		))))
	)
);

const address = 'Сочи, Адлер, ул. Энергетиков, 3';

let data = {
	name: '',
	lname: '',
	phone: '',
	email: '',
	message: '',
}

let nameInput = Input('Имя', z.Ref(data, 'name'), {floatLabel: true});
let lnameInput = Input('Фамилия', z.Ref(data, 'lname'), {floatLabel: true});
let emailInput = Input('Электронная почта', z.Ref(data, 'email'), {floatLabel: true});
let phoneInput = Input('Телефон', z.Ref(data, 'phone'), {floatLabel: true});
let messageInput = Input('Вопрос', z.Ref(data, 'message'), {floatLabel: true});

const ContactUs = z('',
	z('#map'),
	z('.contactUs.columns',
		z('.msg.s0',
			z('h2.roboto', 'Напишите нам'),
			z('.inputs.sp2', nameInput, z('.sz0'), lnameInput),
			z('.inputs', phoneInput, z('.sz0'), emailInput),
			messageInput,
			z('.send', z('i.fa.fa-paper-plane-o'))
		),
		z('.contactInformation',
			z('h2.roboto', 'Как связаться'),
			z('.is-flex.nw.sp2.ac', z('i.fa.fa-map-marker'),
				z('.v', address)
			),
			z('.is-flex.nw.sp1.ac', z('i.fa.fa-mobile'),
				z('.v', '8 (988) 273-36-54')
			),
			z('.is-flex.nw.sp2.ac', z('i.fa.fa-envelope-o'),
				z('.v', 'info@ask-sochi.ru')
			),
			z('.is-flex.nw.sp3.ac',
				z('i.fa.social.fa-vk'),
				z('i.fa.social.fa-facebook'),
				z('i.fa.social.fa-twitter'),
				z('i.fa.social.fa-google-plus')
			)
		)	
	)
);

const Footer = z('.section.sp4.c0',
	z('.container',
		z('.columns.foot',
			z('.column',
				z('.v', 'Design and developed by ', z({is: 'a', href:'http://blackmius.ru'}, 'blackmius'))
			),
			z('.column',
				z('.v', address),
				z('.sp1.is-flex.w.ac.footContacts',
					z('.v', '8 (988) 273-36-54'),
					z('.bullet'),
					z({is: 'a', href: 'mailto:info@ask-sochi.ru'}, 'info@ask-sochi.ru')
				),
				z('.is-flex.w.sp2.ac',
					z('i.fa.social2.fa-vk'),
					z('i.fa.social2.fa-facebook'),
					z('i.fa.social2.fa-twitter'),
					z('i.fa.social2.fa-google-plus')
				)
			)
		)
	)
);

const Body = z('',
	Style, Header, Info, Features, Garanty, HowWeWorks,
	Gallery, ContactUs, Footer, WorkLightroom
);

z.setBody(Body)

z.update(() => {
	let p = [43.424389, 39.946606];
	let map = new L.Map('map', {
		scrollWheelZoom: false,
		attributionControl: false
	}).setView(p, 15);
	L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(map);
	L.marker(p, {
        title: address,
        riseOnHover: true
	}).addTo(map).bindPopup(address);
});

document.title = 'АСК Сочи, аллюминиевые светопрозрачные конструкции, аллюминий Сочи';

});